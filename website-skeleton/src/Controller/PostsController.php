<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Posts;
use App\Entity\Commentaires;
use App\Form\AjoutPostsType;
use App\Form\CommentairesType;
use Symfony\Bridge\Doctrine;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;

class PostsController extends AbstractController
{
    /**
     * @Route("/posts", name="posts")
     */
    public function index()
    {
        //on appelle la liste de tous les posts actifs
        $posts = $this.getDoctrine() -> getRepository(Posts::class) ->findAll(); 
        return $this->render('posts/index.html.twig', [
            'controller_name' => 'PostsController',
        ]);
    }
    public function posts($slug, Request $request){


        $posts = $this->getDoctrine()->getRepository(Posts::class)->findOneBy(['slug' => $slug]);

        $commentaires = $this->getDoctrine()->getRepository(Commentaires::class)->findBy([

            'posts' => $posts,

            'actif' => 1

        ],['created_at' => 'desc']);

        if(!$posts){

            // Si aucun article n'est trouvé, lancement d'une exception

            throw $this->createNotFoundException('L\'article n\'existe pas');

        }


        $commentaire = new Commentaires();
        $form = $this->createForm(CommentairesType::class, $commentaire);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $commentaire->setPosts($posts);
            $commentaire->setCreatedAt(new \DateTime('now'));
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($commentaire);
            $doctrine->flush();

        }

        return $this->render('posts/index.html.twig', [

            'form' => $form->createView(),

            'posts' => $posts,

            'commentaires' => $commentaires,

        ]);

    }

    public function ajout(Request $request,TranslatorInterface $translator)

    {

        $posts = new Posts();



        $form = $this->createForm(AjoutPostsType::class, $posts);

        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {

            $posts->setUsers($this->getUser());

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($posts);

            $entityManager->flush();

            $message = $translator->trans('Article published successfully');

            $this->addFlash('message', $message);

            return $this->redirectToRoute('actualites_articles');

        }
}
}