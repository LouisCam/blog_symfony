<?php



namespace App\Form;


use App\Entity\Posts;

use App\Entity\Categories;

use App\Entity\MotsClefs;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\FileType;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;



class AjoutPostsType extends AbstractType

{

    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder

            ->add('titre', TextType::class)

            ->add('contenu', TextType::class)

            ->add('featured_image', FileType::class, [

                'label' => 'Image'

            ])


            ->add('mots_clefs', EntityType::class, [

                'class' => MotsClefs::class,

                'multiple' => true,

                'expanded' => true

            ])

            ->add('Publier', SubmitType::class)

        ;

    }



    public function configureOptions(OptionsResolver $resolver)

    {

        $resolver->setDefaults([

            'data_class' => Posts::class,

        ]);

    }

}