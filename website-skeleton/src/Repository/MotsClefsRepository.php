<?php

namespace App\Repository;

use App\Entity\MotsClefs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MotsClefs|null find($id, $lockMode = null, $lockVersion = null)
 * @method MotsClefs|null findOneBy(array $criteria, array $orderBy = null)
 * @method MotsClefs[]    findAll()
 * @method MotsClefs[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MotsClefsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MotsClefs::class);
    }

    // /**
    //  * @return MotsClefs[] Returns an array of MotsClefs objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MotsClefs
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
